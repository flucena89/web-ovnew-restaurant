$(window).adaptTo("foundation-registry").register("foundation.validation.validator", {
  selector: "[data-foundation-validation^='multifield-min']",
  validate: function(el) {

    var validationName = el.getAttribute("data-validation")
    var min = validationName.replace("multifield-min-", "");
    min = parseInt(min);

    if (el.items.length < min){
        return "Min allowed items is "+ min
    }
  }
});