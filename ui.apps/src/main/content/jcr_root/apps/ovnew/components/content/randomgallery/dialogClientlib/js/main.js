$(document).on('foundation-contentloaded',function(e){


    if(!$('#modalPreview').length){
        var modal = '<div id="modalPreview">'
                    +	'<img src="" height="320" width="320">'
                    +	'<span class="closePreview">&times;</span>'
					+'</div>';

    	$('.cq-dialog-content').append(modal);
    }



    $('.seeImage').click(function(){
		var srcImage = $(this).parent('section').find('coral-tag input').val();

		$('#modalPreview img').attr('src',srcImage);
        $('#modalPreview').css('display','block');

    });

    $('.closePreview').click(function(){
        $('#modalPreview').css('display','none');

    }); 
});
