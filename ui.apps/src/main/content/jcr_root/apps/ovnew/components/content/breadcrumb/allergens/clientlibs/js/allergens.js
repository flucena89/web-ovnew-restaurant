$(function() {
	var desc="";
	$('.card__allergen').each(function() {
		desc =	$(this).find('.card-body').text().split('\n');
		for ( var i = 0; i < desc.length; i++) {
			desc[i] = desc[i] + '<br>';
		}
		desc = desc.concat('');
		$(this).find('.card-body').html(desc);
	})

});

$('.card__allergen .card-header').hover(function() {
	if ($(this).parent().find('.collapse').hasClass('show')){
		return;
	}else if ($(this).find('i').hasClass('icon_plus')) {
		$(this).find('i').removeClass('icon_plus');
		$(this).find('i').addClass('icon_minus-06');
	}
}, function(){
	if ($(this).parent().find('.collapse').hasClass('show')){
		return;
	}else {
		$(this).find('i').addClass('icon_plus');
		$(this).find('i').removeClass('icon_minus-06');
	}
});

$('.card__allergen .card-header').click(function() {
	if (!$(this).parent().find('.collapse').hasClass('show')){
		$(this).find('i').addClass('icon_minus-06');
		$(this).find('i').removeClass('icon_plus');		
	}else {
		$(this).find('i').addClass('icon_plus');
		$(this).find('i').removeClass('icon_minus-06');
	}
})
