const teaser = $(".cmp-teaser__ovnew .cmp-teaser .cmp-teaser__image ");
const teaserTitle = $('.cmp-teaser__ovnew .cmp-teaser .cmp-teaser__content .cmp-teaser__title');
const teaserTitleContent = $('.cmp-teaser__ovnew .cmp-teaser .cmp-teaser__content');

var lastTitleHeight = teaserTitleContent.height();
var lastTitleWidth = teaserTitle.width();

$(function(){

	var width = $(window).width();
	var titleWidth = teaserTitle.width();
	
	
	if ($(window).width() > 992 ){
		teaser.css("height", width/2.5);
	}else {
		teaser.css("height", "100%");
	}
	
})

$(window).resize(function(){
	
	var width = $(window).width();
	var titleWidth = teaserTitle.width();

	if ($(window).width() > 992 ){
		teaser.css("height", width/2.5);
	}else {
		teaser.css("height", width);
	}
})