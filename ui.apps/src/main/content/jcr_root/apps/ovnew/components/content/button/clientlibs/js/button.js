$(function() {
	const button = $(".button__item--ovnew .cmp-button");
	resize();
	
	$(window).resize(function() {
		if ($(window).width() < "992") {
			button.text("reservar");
		} else {
			button.text("reserva tu viaje");
		}
	})
	
	function resize() {
		if ($(window).width() < "992") {
			button.text("reservar");
		} else {
			button.text("reserva tu viaje");
		}

	}

});

$(window).scroll(function() {
	if ($(window).scrollTop() > 500) {
		$('.button__item--backtotop').css('display', 'block');
	}else {
		$('.button__item--backtotop').css('display', 'none');
	}
});