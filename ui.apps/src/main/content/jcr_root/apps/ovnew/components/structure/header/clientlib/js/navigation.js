$(function() {
	
	const links = $('.container-navigation .cmp-navigation .cmp-navigation__group .cmp-navigation__item .cmp-navigation__item-link');
	const lang = $(".container-rigthAlign .container-rigthAlign-lang .cmp-languagenavigation "
			+ ".cmp-languagenavigation__group .cmp-languagenavigation__item");
	const icon = $(".container-rigthAlign-contact");
	const langGroup = $(".container-rigthAlign .container-rigthAlign-lang");
	
	
	links.each(function() {
	/*	if($(this).find('.cmp-navigation__group')) {
			$(this).find('.cmp-navigation__group').css('display', 'none');
		}*/
		if ($(this).attr('title').toLowerCase() == 'reservas'){
			$(this).appendTo('.container-rigthAlign');
		}
	});
	
	lang.each(function() {
		if (!$(this).hasClass("cmp-languagenavigation__item--active")) {
			$(this).css("display", "none");
		}
	});


	langGroup.hover(showLangList, hideLangList);

	function showLangList() {
		
		var count = 0;
		var flag = false;
		if ($(window).scrollTop() > 200){
			var a = 70;
			flag = true;
		}else{
			a = 123;
			flag = false;
		}
		for (let i = 0; i < lang.length; i++) {
			
			if (!lang[i].classList
					.contains("cmp-languagenavigation__item--active")) {
				count= count +1 ;
				lang[i].style.display = "flex";
				lang[i].style.width = "80px";
				lang[i].style.backgroundColor = "black";
				if (flag){
					if (count >= 2 ){
						a = a +63;
						lang[i].style.top = a +"px";
					}else {
						lang[i].style.top = "70px";
						
					}
				}else{
					if (count >= 2 ){
						a = a +63;
						lang[i].style.top = a +"px";
					}else {
						lang[i].style.top = "123px";
						//a=70;
					}
				}
				
				
				lang[i].style.position = "absolute";
				lang[i].style.justifyContent = "center";
				lang[i].style.paddingTop = "20px";
				lang[i].style.paddingBottom = "20px";

				var child = lang[i].children;
				for (let i = 0; i < child.length; i++) {
					if (child[i].classList.contains("cmp-languagenavigation__item-link")) {
						child[i].style.color = "white";
					}
				}
			}
		}
	}
	function hideLangList() {
		for (let i = 0; i < lang.length; i++) {
			if (!lang[i].classList
					.contains("cmp-languagenavigation__item--active")) {
				lang[i].style.display = "none";
			}
		}
	}

	icon.hover(function() {
		if ($(window).width() > "992") {
			if ($('.wknd-header.scrolly').css('top')< '0px'){
				$(".container-rigthAlign-contact-number.noScroll").css("display", "block");
				$(".container-rigthAlign-contact-number.noScroll").css("position", "absolute");
			}else {
				$(".container-rigthAlign-contact-number").css("display", "block");
				$(".container-rigthAlign-contact-number").css("position", "absolute");
			}
			
		}
	}, function() {
		$(".container-rigthAlign-contact-number").css("display", "none");
	});



	if ($(".navigation--mobile-separator").length == 0) {
		if ($(window).width() <= "992") {
			$("<hr class='navigation--mobile-separator'>").insertAfter(
					".navigation--mobile-navigation .cmp-navigation__group .cmp-navigation__item");
		} else {
			$(".navigation--mobile-separator").remove();
		}
	}

});
function closeNav() {
	$(".navigation--side").width(0);
}
function showNav() {
	if ($(window).width() <= "992") {
		$(".header").css("position", "relative");
		if ($(".navigation--mobile").height() == "0") {
			if ($('.wknd-header.scrolly').css("top") == "0px"){
				$(".navigation--mobile").height(317);
				$(".container-rigthAlign-menu-icon").css("color", "red");
				$(".navigation--mobile").css("position", "fixed");
			}else {
				$(".navigation--mobile").height(317);
				$(".container-rigthAlign-menu-icon").css("color", "red");
			}

		} else {
			$(".navigation--mobile").height(0);
			$(".container-rigthAlign-menu-icon").css("color", "black");
			
		}

	} else {
		$(".navigation--side").width(425);
	}
}

$(document).ready(function() {
	var header = $(".wknd-header "), clone = header.before(header.clone().addClass("scrolly"));
    var lastScroll = 0;
    clone.find('.container-rigthAlign-contact-number').addClass('noScroll');
    $('.wknd-header.scrolly').find('.container-rigthAlign-contact-number').addClass('scroll');
	$(window).scroll(function(){
	    scroll = $(window).scrollTop();
	    
	    const scrollyHeaderContact = $('.wknd-header.scrolly').find('.container-rigthAlign-contact');
	    
		scrollyHeaderContact.hover(function() {
			if ($(window).width() > "992") {
				if ($('.wknd-header.scrolly').css('top') == '0px'){
					$(".container-rigthAlign-contact-number.scroll").css("display", "block");
					$(".container-rigthAlign-contact-number.scroll").css("position", "absolute");
					$(".container-rigthAlign-contact-number.scroll").css("top", "80px");
				}
			}
		},function(){
			$(".container-rigthAlign-contact-number").css("display", "none");
		});
	    $('body').toggleClass("down", (scroll > 1000));
    
	    
    	if ($(window).width() <= "992") {
    	    if (scroll == 0){
    	    	$('.wknd-header.scrolly').css("top", "-80px");
    	    }
    		if (scroll < lastScroll && scroll > 0) {
				$('.wknd-header.scrolly').css("top", "0px");    			
    		}else {
    			if ($(".navigation--mobile").height() == "0") {
    				$('.wknd-header.scrolly').css("top", "-80px");
    			}else {
    				$('.wknd-header.scrolly').css("top", "0px");
    				
    			}
    			
    		}
    		lastScroll = scroll;
    	}else {
		    if (scroll > 0 && scroll <= 300) {
				if ($(".navigation--mobile").height() == "317") {
					$('.wknd-header.scrolly').css("top", "0px");
					$(".navigation--mobile").css("position", "fixed");
				}
		    } else if(scroll > 300 ) {
		        $('.wknd-header.scrolly').css("top", "0px");
		    } else {
		    	$('.wknd-header.scrolly').css("top", "-80px");
		    }
    	}
	 })
});
$(function() {
	$('.wknd-header.scrolly .cmp-navigation').clone().appendTo('.navigation--side-navigation');
	$('.navigation--side-navigation .cmp-navigation__item').each(function() {
		if (!$(this).children().html()){
			$('.wknd-header.scrolly .container-rigthAlign .cmp-navigation__item-link').clone().appendTo($(this));
		}
	})
})
