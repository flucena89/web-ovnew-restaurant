/** Circular Carousel Variables*/
const carouselImages = $('#cmp-circularCarousel .cmp-circularCarousel__images .cmp-circularCarousel__image');
var imagesLength = carouselImages.length;
var slide = $('#cmp-circularCarousel .cmp-circularCarousel__images .cmp-circularCarousel__image');
var length = slide.length;
/**
 * Function to build the 3D carousel on load
 */
$(function(){
	if (imagesLength > 0) {
		for (var i = 0; i<= imagesLength; i++){
			if (i== 1) {
				carouselImages[i].classList.add('current');
				$('#s1').prop('checked', true);
			}
			if (i==0) {
				carouselImages[i].classList.add('prev');
			}
			if (i==2) {
				carouselImages[i].classList.add('next');
			}
			if (i == 3) {
				carouselImages[i].classList.add('restleft');
			}
			if (i > 3 && i< imagesLength){
				carouselImages[i].classList.add('restback');
			}
			if (i == imagesLength-1){
				carouselImages[i].classList.remove('restback');
				carouselImages[i].classList.add('restright');
			}
		}
	}
});

/**
 * Functions to slide images on the carousel*/
function nextImage() {
	slide.each( function() {
		if ( $(this).hasClass('current') ){			
			var id = $(this).attr('id').replace('slide','');
			
			var prev = parseInt(id) -1;
			if (prev < 0) {
				prev = length-1;
			}
			var newCurrent = parseInt(id) +1;
			var restback;
			var restleft;
			//If current image is the last of one
			if (newCurrent >= length) {
				newCurrent = 0;
			}
			var next = newCurrent +1;
			if (next >= length) {
				next = 0;
			}
			restleft = next +1;
			if ( restleft >= length) {
				restleft = 0;
			}
			var restright = prev -1;
			if (restright < 0) {
				restright = length -1;
			}
			restback = restleft +1;
			if (restback >= length) {
				restback = 0;
			}
			
			/*current image*/
			$(this).removeClass('current');
			$(this).addClass('prev');
			/*previous image*/
			$('#slide'+prev).removeClass('prev');
			$('#slide'+prev).removeClass('next');
			$('#slide'+prev).removeClass('restright');
			$('#slide'+prev).removeClass('restleft');
			$('#slide'+prev).removeClass('restback');
			$('#slide'+prev).addClass('restright');
			/*new current image*/
			$('#slide'+newCurrent).removeClass('next');
			$('#slide'+newCurrent).removeClass('restleft');
			$('#slide'+newCurrent).removeClass('restright');
			$('#slide'+newCurrent).removeClass('restback');
			$('#slide'+newCurrent).addClass('current');
			$('#s'+newCurrent).prop('checked', true);
			/*next image*/
			$('#slide'+next).removeClass('next');
			$('#slide'+next).removeClass('prev');
			$('#slide'+next).removeClass('restleft');
			$('#slide'+next).removeClass('restright');
			$('#slide'+next).removeClass('restback');
			$('#slide'+next).addClass('next');
			/*rest left image*/
			$('#slide'+restleft).removeClass('next');
			$('#slide'+restleft).removeClass('prev');
			$('#slide'+restleft).removeClass('restleft');
			$('#slide'+restleft).removeClass('restright');
			$('#slide'+restleft).removeClass('restback');
			$('#slide'+restleft).addClass('restleft');
			/*rest back images*/
			$('#slide'+restback).removeClass('next');
			$('#slide'+restback).removeClass('prev');
			$('#slide'+restback).removeClass('restleft');
			$('#slide'+restback).removeClass('restright');
			$('#slide'+restback).removeClass('restback');
			$('#slide'+restback).addClass('restback');
			
			return false;//return to break cicle
		}
	});
};
function prevImage() {
	slide.each( function() {
		if ( $(this).hasClass('current') ){			
			var id = $(this).attr('id').replace('slide','');
			
			var next = parseInt(id) +1;
			if (next >= length) {
				next=0;
			}
			var restback;
			var restleft;
			
			var newCurrent = parseInt(id) -1;
			if (newCurrent < 0) {
				newCurrent = length -1;
			}
			var prev = newCurrent -1;
			if (prev < 0){
				prev = length -1;
			}
			restleft = next +1;
			if ( restleft >= length) {
				restleft = 0;
			}
			restback = restleft +1;
			if (restback >= length) {
				restback = 0;
			}
			/*current image*/
			$(this).removeClass('current');
			$(this).addClass('next');
			/*previous image*/
			$('#slide'+prev).removeClass('prev');
			$('#slide'+prev).removeClass('next');
			$('#slide'+prev).removeClass('restright');
			$('#slide'+prev).removeClass('restleft');
			$('#slide'+prev).removeClass('restback');
			$('#slide'+prev).addClass('prev');
			/*new current image*/
			$('#slide'+newCurrent).removeClass('next');
			$('#slide'+newCurrent).removeClass('restleft');
			$('#slide'+newCurrent).removeClass('restright');
			$('#slide'+newCurrent).removeClass('restback');
			$('#slide'+newCurrent).removeClass('prev');
			$('#slide'+newCurrent).addClass('current');
			$('#s'+newCurrent).prop('checked', true);
			/*next image*/
			$('#slide'+next).removeClass('next');
			$('#slide'+next).removeClass('prev');
			$('#slide'+next).removeClass('restleft');
			$('#slide'+next).removeClass('restright');
			$('#slide'+next).removeClass('restback');
			$('#slide'+next).addClass('restleft');
			/*rest left image*/
			$('#slide'+restleft).removeClass('next');
			$('#slide'+restleft).removeClass('prev');
			$('#slide'+restleft).removeClass('restleft');
			$('#slide'+restleft).removeClass('restright');
			$('#slide'+restleft).removeClass('restback');
			$('#slide'+restleft).addClass('restright');
			/*rest back images*/
			$('#slide'+restback).removeClass('next');
			$('#slide'+restback).removeClass('prev');
			$('#slide'+restback).removeClass('restleft');
			$('#slide'+restback).removeClass('restright');
			$('#slide'+restback).removeClass('restback');
			$('#slide'+restback).addClass('restback');
			return false;//return to break cicle
		}
	});
}

function slideImage(image){
	if (!image.classList.contains('current')){
		var currentImage = image.parentNode.getElementsByClassName('current')[0].id;
		$('#'+currentImage).removeClass('current');
		
		if (image.classList.contains('restright') || image.classList.contains('next')) {
			var id = parseInt(image.id.replace('slide','')) -1;
			if (id < 0 ) {
				id = length-1;
			}
			$('#slide'+id).removeClass('next');
			$('#slide'+id).removeClass('prev');
			$('#slide'+id).removeClass('restback');
			$('#slide'+id).removeClass('restright');
			$('#slide'+id).removeClass('restleft');
			$('#slide'+id).addClass('current');
			nextImage();
		}else if (image.classList.contains('restleft') || image.classList.contains('prev')) {
			var id = parseInt(image.id.replace('slide','')) +1;	
			if (id >= length ) {
				id = 0;
			}
			$('#slide'+id).removeClass('next');
			$('#slide'+id).removeClass('prev');
			$('#slide'+id).removeClass('restback');
			$('#slide'+id).removeClass('restright');
			$('#slide'+id).removeClass('restleft');
			$('#slide'+id).addClass('current');
			prevImage();
		}
	}
}
