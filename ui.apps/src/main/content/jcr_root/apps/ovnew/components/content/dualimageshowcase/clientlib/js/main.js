$(document).ready(function(){
		
	var subtitleColor = $('.content__subtitle').data('color');
	var titleColor = $('.content__title').data('color');
	var titleMarginTop = $('.content__title').data('margintop');
	var titleMarginBottom = $('.content__title').data('marginbottom');
	var textColor = $('.content__text').data('color');
	var textMarginTop = $('.content__text').data('margintop');
	var enableParallax = $('.images').data('parallax');
	var axisBackground = 0;
	var axisForeground = 0;
	if(enableParallax == 'yes'){
		axisBackground = $('.background').data('axis');
		axisForeground = $('.foreground').data('axis');
	}else{
		axisBackground = 0;
		axisForeground = 0;
	}
		
	$('.content__subtitle').css('color',subtitleColor);
	
	$('.content__title').css('color',titleColor);
	$('.content__title').css('margin-top',titleMarginTop);
	$('.content__title').css('margin-bottom',titleMarginBottom);
	
	$('.content__text p').css('color',textColor);
	$('.content__text').css('margin-top',textMarginTop);

	var
    stylesheet = document.styleSheets[0] // replace 0 with the number of the stylesheet that you want to modify
  , rules = stylesheet.rules
  , rulesex = stylesheet.rules
  , i = rules.length
  , j = rules.length
  , keyframesBack
  , keyframeBack
  , keyframesFore
  , keyframeFore;
	
	while (i--) {
	    keyframesBack = rules.item(i);
	    if (
	        (
	               keyframesBack.type === keyframesBack.KEYFRAMES_RULE
	            || keyframesBack.type === keyframesBack.WEBKIT_KEYFRAMES_RULE
	        )
	        && keyframesBack.name === "floatBack"
	    ) {
	        rules = keyframesBack.cssRules;
	        i = rules.length;
	        while (i--) {
	            keyframeBack = rules.item(i);
	            
	            if (
	                (
	                       keyframeBack.type === keyframeBack.KEYFRAME_RULE
	                    || keyframeBack.type === keyframeBack.WEBKIT_KEYFRAME_RULE
	                )
	                && keyframeBack.keyText === "50%"
	            ) {
	                keyframeBack.style.webkitTransform =
	                keyframeBack.style.transform =
	                    "translatey("+ axisBackground +"px)";
	                break;
	            }
	        }
	        break;
	    }
	}
	
	while (j--) {
		keyframesFore = rulesex.item(j);
	   
	    if (
	        (
	               keyframesFore.type === keyframesFore.KEYFRAMES_RULE
	            || keyframesFore.type === keyframesFore.WEBKIT_KEYFRAMES_RULE
	        )
	        && keyframesFore.name === "floatFore"
	    ) {
	        rulesex = keyframesFore.cssRules;
	        j = rulesex.length;
	        while (j--) {
	            keyframeFore = rulesex.item(j);
	            if (
	                (
	                       keyframeFore.type === keyframeFore.KEYFRAME_RULE
	                    || keyframeFore.type === keyframeFore.WEBKIT_KEYFRAME_RULE
	                )
	                && keyframeFore.keyText === "50%"
	            ) {
	                keyframeFore.style.webkitTransform =
	                keyframeFore.style.transform =
	                    "translatey("+ axisForeground +"px)";
	                break;
	            }
	        }
	        break;
	    }
	}
	
});