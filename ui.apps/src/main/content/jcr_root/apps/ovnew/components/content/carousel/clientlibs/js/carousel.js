$(function() {
	const indicators = $(".cmp-carousel__indicators");
	indicators.css("display", "none");
	$('.cmp-carousel__item').each(function() {
		var teaser = $(this).find('.cmp-teaser__content');
		if (teaser.find('h2').text() == ''){
			teaser.css('display', 'none');
		}

	})

		$('.cmp-carousel--fullWidth .cmp-carousel__action-text').text('');
		$('.cmp-carousel--fullWidth .cmp-carousel__action--previous .cmp-carousel__action-text').addClass('arrow_carrot-left');
		$('.cmp-carousel--fullWidth .cmp-carousel__action--next .cmp-carousel__action-text').addClass('arrow_carrot-right');

})
