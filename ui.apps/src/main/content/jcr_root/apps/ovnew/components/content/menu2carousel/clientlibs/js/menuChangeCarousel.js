$(function($) {
	if (document.getElementsByClassName('menu2carousel')[0]){
		var normalHeight;
		var resizedHeight;
		var indicadorActivo = $('.menu2carousel .cmp-carousel__item--active');
		function sideEfect(){
			   /*
			    * calculo del punto medio del carousel para la aparicion de los iconos. 
			    * en base a la altura de toda la pagina - Formula:
			    * height del carrusel/2(punto medio) + posicion en la que comienza el carrusel
			    */
			if (document.getElementsByClassName('menu2carousel')[0]){
			var scroll = $(window).scrollTop()+$(window).innerHeight();
				var scrollMax = document.getElementsByClassName('menu2carousel')[0].offsetHeight/2
						+document.getElementsByClassName('menu2carousel')[0].offsetTop+20;
			   if(scroll >= scrollMax) {
			       $('.sideImage')[0].style.animationPlayState = "running";
			   }
			}
		}
		sideEfect();
		/*
		 * inicio
		 * bloque de codigo para hacer que el tamanio de todos los menus
		 * coincidan 
		 */
		
		var ajuste;
		if (document.getElementsByClassName('menu2carousel')[0]){
			if (document.getElementById("heightAdjust").value == ""){
				ajuste = 0;
			}else{
				ajuste = parseInt(document.getElementById("heightAdjust").value);
			}
		    var mayor = 0;
		    var longitud = $('.content').length;
		    for (var i=0; i<longitud;i++ ){
		    	if ($('.content')[i].children.length > mayor){
		    		mayor = $('.content')[i].children.length;
		    	}
		    }
		    var pixeles = (mayor*150)+350+ajuste;//altura media de cada plato + titulo del menu;
		    $('.internalMenu').height(pixeles);
		    normalHeight = pixeles;
		    resizedHeight = pixeles*1.1;//llevarlo al 110% de su tamaño normal
		}
	  /*
	   * fin del bloque
	   */
	   /*******************************************************************
	    * efecto de entrada para los iconos laterales al carousel del menu*
	    *******************************************************************/
	   $(window).scroll(function(){
		   sideEfect();
	   });
	   $(window).bind('resize orientationchange', function(){
		   if (window.innerWidth < 450){
			   $('.internalMenu').height(resizedHeight);
		   }else{
			   $('.internalMenu').height(normalHeight);
		   }
		});
	   function clearClass(){
		   $('.menu2carousel .cmp-carousel__item').removeClass("cmp-carousel--reverseOut");
		   $('.menu2carousel .cmp-carousel__item').removeClass("cmp-carousel--reverseIn");
		   $('.menu2carousel .cmp-carousel__item').removeClass("cmp-carousel--nextIn");
		   $('.menu2carousel .cmp-carousel__item').removeClass("cmp-carousel--nextOut");
	   }
	   /******************************************************************
	    * efecto de la transicion de carruseles por boton y por indicador*
	    ******************************************************************/
	   /* 
	    * al ingresar a esta funcion aun no se a cambiado el elemento activo a la clase correspondiente
	    * boton next
	    */
	   $(".menu2carousel .cmp-carousel__action--next")[0].onclick = function(){
		   clearClass();
		   $('.menu2carousel .cmp-carousel__item--active').addClass("cmp-carousel--nextOut");
		   if ($('.menu2carousel .cmp-carousel__item--active')[0].nextElementSibling.className == "cmp-carousel__item"){
			   $('.menu2carousel .cmp-carousel__item--active')[0].nextElementSibling.className = "cmp-carousel__item cmp-carousel--nextIn";
			   indicadorActivo = $('.menu2carousel .cmp-carousel__item--active').next();
		   }else{
			   $('.menu2carousel .cmp-carousel__item--active')[0].parentNode.firstElementChild.nextElementSibling.className ="cmp-carousel__item cmp-carousel--nextIn";
			   indicadorActivo = $('.menu2carousel .cmp-carousel__item--active').parent().children().first().next();
		   }
	   };
	   /* 
	    * al ingresar a esta funcion aun no se a cambiado el elemento activo a la clase correspondiente
	    * boton previo
	    */
	   $(".menu2carousel .cmp-carousel__action--previous")[0].onclick = function(){
		   clearClass();
		   $('.menu2carousel .cmp-carousel__item--active').addClass("cmp-carousel--reverseOut");
		   if ($('.menu2carousel .cmp-carousel__item--active')[0].previousElementSibling.className == "cmp-carousel__item"){
			   $('.menu2carousel .cmp-carousel__item--active')[0].previousElementSibling.className = "cmp-carousel__item cmp-carousel--reverseIn";
			   indicadorActivo = $('.menu2carousel .cmp-carousel__item--active').prev();
		   }else{
			   var childrens = $('.menu2carousel .cmp-carousel__item--active')[0].parentNode.childElementCount;
			   $('.menu2carousel .cmp-carousel__item--active')[0].parentNode.lastElementChild.previousElementSibling.
			   		previousElementSibling.previousElementSibling.className = "cmp-carousel__item cmp-carousel--reverseIn";
			   indicadorActivo = $('.menu2carousel .cmp-carousel__item--active').parent().children().last().prev().prev().prev();
		   } 
	   };
	   /* 
	    * al ingresar a esta funcion el elemento activo es el correspondiente
	    * por indicadores debajo del carousel
	    */
	   $(".menu2carousel .cmp-carousel__indicators")[0].onclick = function(){
		   clearClass();
		   var reversa = false;
		   var actual = indicadorActivo[0];
		   while (!reversa 
				   && actual.nextElementSibling != $('.menu2carousel .cmp-carousel__item--active')[0]
					   && actual.nextElementSibling != null){
			   actual = actual.nextElementSibling;
			   if (actual.className == "cmp-carousel__hideSides cmp-carousel__hideSides--left"){
				   reversa = true;
			   }
		   }
		   if (reversa){
			   indicadorActivo.addClass(".menu2carousel cmp-carousel--reverseOut");
			   $('.menu2carousel .cmp-carousel__item--active').addClass("cmp-carousel--reverseIn");
		   }else{
			   indicadorActivo.addClass(".menu2carousel cmp-carousel--nextOut");
			   $('.menu2carousel .cmp-carousel__item--active').addClass("cmp-carousel--nextIn");
		   }
		   
		   indicadorActivo = $(".menu2carousel .cmp-carousel__item--active");
	   };
	}
});
var actualScreen;
function mouseOut(e){
	if (e.screenX != actualScreen){
	   if (e.screenX < actualScreen-100){
		   $(".menu2carousel .cmp-carousel__action--next").click()
	   }
	   if (e.screenX > actualScreen-100){
		   $(".menu2carousel .cmp-carousel__action--previous").click()
	   }
	}
	   //console.log(window.screen)
}

function mouseIn(e){
	   actualScreen = e.screenX;
}