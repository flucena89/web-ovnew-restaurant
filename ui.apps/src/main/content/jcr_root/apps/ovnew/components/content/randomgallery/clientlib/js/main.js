//Inicialización de variables
var img_activa=[];
var img_desactiva=[];
var gallery = [];
var gallery_desort=[];
var controlador = null;
var controlador_a = null;
var controlador_b = null;
var rotateRowOneTwo = true;
var loadPictureOne = false;
var loadPictureTwo = false;
var loadPictureThree = false;
var loadPictureFour = false;
var loadPictureFive = false;
var loadPictureSix = false;

$(document).ready(function(){
		
	if(!$('div').hasClass('cmp-random__container')){
		return
	}
	
	gallery = $('.cmp-random__container').data('resource').split('@#key#@,');
	
	//Imágenes activas inicialmente
	img_activa=gallery.slice(0, 6);
	
	//Imágenes no activas inicialmente
	img_desactiva=gallery.slice(6,gallery.length);
		
	//Asigna las 6 imágenes
	$('#picture-one .first img').each(function(index, el) {
		$(this).attr('src', img_activa[0]);
	});
	$('#picture-one .first img').on('load',function() {
		loadPictureOne=true;
	});			

	$('#picture-two .first img').each(function(index, el) {
		$(this).attr('src', img_activa[1]);
	});
	$('#picture-two .first img').on('load',function() {
		loadPictureTwo=true;
	});		
	
	$('#picture-three .first img').each(function(index, el) {
		$(this).attr('src', img_activa[2]);
	});
	$('#picture-three .first img').on('load',function() {
		loadPictureThree=true;
	});
	
	$('#picture-four .first img').each(function(index, el) {
		$(this).attr('src', img_activa[3]);
	});
	$('#picture-four .first img').on('load',function() {
		loadPictureFour=true;
	});

	$('#picture-five .first img').each(function(index, el) {
		$(this).attr('src', img_activa[4]);
	});
	$('#picture-five .first img').on('load',function() {
		loadPictureFive=true;
	});				

	$('#picture-six .first img').each(function(index, el) {
		$(this).attr('src', img_activa[5]);
	});
	$('#picture-six .first img').on('load',function() {
		loadPictureSix=true;
	});
	
	
	if (screen.width<991) {
		//Limpia Interval y Timeout
		clearInterval(controlador);
		clearTimeout(controlador_a);
		clearTimeout(controlador_b);
	}
	
	$(".owl-demo").owlCarousel({
		nav : false, // Show next and prev buttons
		singleItem:true,
		items:1,
		dots:false,
		dragEndSpeed:false,
		touchDrag: false,
		mouseDrag: false,
		lazyLoad:true,
		loop:true,
		smartSpeed:3500
  	});
	
	$('.owl-demo').click(function() {

  		if (screen.width>991) {
  			//Limpia Interval y Timeout
	  		clearInterval(controlador);
	  		clearTimeout(controlador_a);
	  		clearTimeout(controlador_b);

	  		var classRow = '';

	  		if ($(this).hasClass('row-one')) {
	  			classRow = '.row-one';
	  			rotateRowOneTwo = false;
	  			
	  		}else{
	  			classRow = '.row-two';
	  			rotateRowOneTwo = true;
	  		} 		

			$(classRow).each(function(index, el) {
				//Obtener id
		  		var id= '#' + $(this).attr('id');
		  		//Obtengo src imagen actual
				var img_actual='';
				var itemActive='';				

				$(id).find('.owl-item').each(function(index, el) {
					if ($(this).hasClass('active')) {
						img_actual = $(this).find('.item img').attr('src');
						if ($(this).find('.item').hasClass('first')) {
							itemActive='first';
						}else{
							itemActive='second';
						}
					}
				});

				//Obtengo posición de imagen actual
				var pos_actual = img_activa.indexOf(img_actual);
				//Elimino imagen actual del array de imágenes activas
				img_activa.splice(pos_actual,1);
				//Obtengo la próxima imagen que se mostrará
				var img_next = img_desactiva[0];
				//Elimino próxima imagen del array de imagenes desactivas
				img_desactiva.splice(0,1);
				//Agrego próxima imagen a mostrar en el array de imágenes activas
				img_activa.push(img_next);
				//Agregó la imagen anterior al array de imágenes no activas
				img_desactiva.push(img_actual);

				//Actualizo la imagen en la vista
				if (itemActive=='first') {
					var rootSecond = id + ' .second img';
					$(rootSecond).each(function(index, el) {
						$(this).attr('src',img_next);
					});
				}else{
					var rootFirst = id + ' .first img';
					$(rootFirst).each(function(index, el) {
						$(this).attr('src',img_next);
					});
				}
			});

	  		var owl = $(classRow);
	    	owl.trigger('prev.owl.carousel');

	    	//Rota imagen siguiente despues de 2seg de haber hecho click sobre una imagen
	    	controlador_a = setTimeout(function(){
					rotateRow();
			},3500);
  		}
	});
	
	//Inicialmente rota imágenes de forma automatica
 	controlador = setInterval(function(){
 		if (loadPictureOne && loadPictureTwo && loadPictureThree && loadPictureFour && loadPictureFive && loadPictureSix) {
	 		
	 		var classRow = '';
	 		
	 		if (rotateRowOneTwo) {
	 			classRow='.row-one';
	 			rotateRowOneTwo = false;

	 		}else{
	 			classRow='.row-two';
	 			rotateRowOneTwo = true;
	 		}

	 		$(classRow).each(function(index, el) {
				//Obtener id
		  		var id= '#' + $(this).attr('id');
		  		//Obtengo src imagen actual
				var img_actual='';
				var itemActive='';				

				$(id).find('.owl-item').each(function(index, el) {
					if ($(this).hasClass('active')) {
						img_actual = $(this).find('.item img').attr('src');
						if ($(this).find('.item').hasClass('first')) {
							itemActive='first';
						}else{
							itemActive='second';
						}
					}
				});


				//Obtengo posición de imagen actual
				var pos_actual = img_activa.indexOf(img_actual);
				//Elimino imagen actual del array de imágenes activas
				img_activa.splice(pos_actual,1);
				//Obtengo la próxima imagen que se mostrará
				var img_next = img_desactiva[0];
				//Elimino próxima imagen del array de imagenes desactivas
				img_desactiva.splice(0,1);
				//Agrego próxima imagen a mostrar en el array de imágenes activas
				img_activa.push(img_next);
				//Agregó la imagen anterior al array de imágenes no activas
				img_desactiva.push(img_actual);

				//Actualizo la imagen en la vista
				if (itemActive=='first') {
					var rootSecond = id + ' .second img';
					$(rootSecond).each(function(index, el) {
						$(this).attr('src',img_next);
					});
				}else{
					var rootFirst = id + ' .first img';
					$(rootFirst).each(function(index, el) {
						$(this).attr('src',img_next);
					});
				}
			});

	  			var owl = $(classRow);
	  			if(screen.width>991){
	  				owl.trigger('prev.owl.carousel');
	  			}
	  			

	 		}
 		},3500);
	
});

function rotateRow(){
	//Limpia Interval y Timeout
  	clearInterval(controlador);
  	clearTimeout(controlador_a);
  	clearTimeout(controlador_b);
	if (rotateRowOneTwo) {
		classRow='.row-one';
		rotateRowOneTwo = false;
		
	}else{
		classRow='.row-two';
		rotateRowOneTwo = true;
		
 	}

	$(classRow).each(function(index, el) {
		//Obtener id
		var id= '#' + $(this).attr('id');
		//Obtengo src imagen actual
		var img_actual='';
		var itemActive='';				

		$(id).find('.owl-item').each(function(index, el) {
			if ($(this).hasClass('active')) {
				img_actual = $(this).find('.item img').attr('src');
				if ($(this).find('.item').hasClass('first')) {
					itemActive='first';
				}else{
					itemActive='second';
				}
			}
		});

		//Obtengo posición de imagen actual ...........
		var pos_actual = img_activa.indexOf(img_actual);
		//Elimino imagen actual del array de imágenes activas
		img_activa.splice(pos_actual,1);
		//Obtengo la próxima imagen que se mostrará
		var img_next = img_desactiva[0];
		//Elimino próxima imagen del array de imagenes desactivas
		img_desactiva.splice(0,1);
		//Agrego próxima imagen a mostrar en el array de imágenes activas
		img_activa.push(img_next);
		//Agregó la imagen anterior al array de imágenes no activas
		img_desactiva.push(img_actual);

		//Actualizo la imagen en la vista
		if (itemActive=='first') {
			var rootSecond = id + ' .second img';
			$(rootSecond).each(function(index, el) {
				$(this).attr('src',img_next);
			});
		}else{
			var rootFirst = id + ' .first img';
			$(rootFirst).each(function(index, el) {
				$(this).attr('src',img_next);
			});
		}
	});

	var owl = $(classRow);
	owl.trigger('prev.owl.carousel');

	controlador_b = setTimeout(function(){
			rotateRow();
	},3500);
}