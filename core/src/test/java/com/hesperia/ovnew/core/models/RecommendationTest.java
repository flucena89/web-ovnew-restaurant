package com.hesperia.ovnew.core.models;

import static org.junit.jupiter.api.Assertions.*;

import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class RecommendationTest {
	public final AemContext ctx = new AemContext();

	@BeforeEach
	void setUp() throws Exception {
		ctx.load().json("/com/hesperia/ovnew/core/models/RecommendationTest.json", "/content");
	}
	
	@Test 
	void constructors(){
		Recommendation parameters = new Recommendation("/content/dam/theovnewicons/2-ovnew-08.png", "Titulo de la recomendacion", "Descripcion de la recomendacion");
		Resource res = ctx.currentResource("/content/rec");
		Recommendation auto = new Recommendation(res.adaptTo(Recommendation.class));
		
		assertEquals(parameters.toString(), auto.toString());
	}
	
	@Test
	void testGetIcon() {
		String expected = "/content/dam/theovnewicons/2-ovnew-08.png";
		Resource res = ctx.currentResource("/content/rec");
		Recommendation rec = res.adaptTo(Recommendation.class);
		String actual = rec.getIcon();
		
		assertEquals(expected, actual);
	}

	@Test
	void testGetTitle() {
		String expected = "Titulo de la recomendacion";
		
		Resource res = ctx.currentResource("/content/rec");
		Recommendation rec = res.adaptTo(Recommendation.class);
		String actual = rec.getTitle();
		
		assertEquals(expected, actual);
	}

	@Test
	void testGetDescription() {
		String expected = "Descripcion de la recomendacion";
		
		Resource res = ctx.currentResource("/content/rec");
		Recommendation rec = res.adaptTo(Recommendation.class);
		String actual = rec.getDescription();
		
		assertEquals(expected, actual);
	}

	@Test
	void testIsEmpty() {
		boolean expected = true;
		Resource res = ctx.currentResource("/content/recEmpty");
		Recommendation rec = res.adaptTo(Recommendation.class);
		boolean actual = rec.isEmpty();
		
		assertEquals(expected, actual);
	}
	
	@Test
	void testIsNotEmpty() {
		boolean expected = false;
		Resource res = ctx.currentResource("/content/rec");
		Recommendation rec = res.adaptTo(Recommendation.class);
		boolean actual = rec.isEmpty();
		
		assertEquals(expected, actual);
	}
	
	@Test
	void testNotEmptyWithDescription() {
		boolean expected = false;
		Resource res = ctx.currentResource("/content/emptyWithDescription");
		Recommendation rec = res.adaptTo(Recommendation.class);
		boolean actual = rec.isEmpty();
		
		assertEquals(expected, actual);
	}

}
