package com.hesperia.ovnew.core.models;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class PlatosTest {
	public final AemContext ctx = new AemContext();

	@BeforeEach
	void setUp() throws Exception {
		ctx.load().json("/com/hesperia/ovnew/core/models/PlatosTest.json", "/content");
	}

	@Test
	void Construct() {
		ArrayList<String> allergen = new ArrayList<>();
		allergen.add("alergeno1");
		allergen.add("alergeno2");
		allergen.add("alergeno3");
		allergen.add("alergeno4");
		float cost = 150;
		Platos expected = new Platos("SER PRIMIGENIO", "El gusto y sus inicios", "- Cogollo a la brasa, hierbas y flores. Vinagreta oriental\n- Bulbos, tubérculos, cereales y fermentos\n- Crujiente de maíz\n-moringa, kimchi de rábanos y cerezas",
				cost, "/content/dam/theovnewicons/gaia.png", allergen);
		Resource res = ctx.currentResource("/content/lleno");
		Platos actual = new Platos(res.adaptTo(Platos.class));
		
		assertEquals(expected.toString(), actual.toString());
	}
	@Test
	void testGetTitulo() {
		final String expected = "SER PRIMIGENIO";
		Resource res = ctx.currentResource("/content/lleno");
		Platos data = res.adaptTo(Platos.class);

		String actual = data.getTitulo();
		assertEquals(expected, actual);
	}

	@Test
	void testGetDescripcion() {
		final String expected = "El gusto y sus inicios";
		Resource res = ctx.currentResource("/content/lleno");
		Platos data = res.adaptTo(Platos.class);

		String actual = data.getDescripcion();
		assertEquals(expected, actual);
	}

	@Test
	void testGetDetalles() {
		List<String> expected = new ArrayList<>();
		expected.add("- Cogollo a la brasa, hierbas y flores. Vinagreta oriental");
		expected.add("- Bulbos, tubérculos, cereales y fermentos");
		expected.add("- Crujiente de maíz");
		expected.add("-moringa, kimchi de rábanos y cerezas");
		Resource res = ctx.currentResource("/content/lleno");
		
		Platos data = res.adaptTo(Platos.class);

		List<String> actual = data.getDetalles();
		assertEquals(expected, actual);
	}

	@Test
	void testGetCosto() {
		final float expected = 150;
		Resource res = ctx.currentResource("/content/lleno");
		Platos data = res.adaptTo(Platos.class);

		float actual = data.getCosto();
		assertEquals(expected, actual);
	}
	
	@Test
	void testGetCostoCero() {
		final float expected = 0;
		Resource res = ctx.currentResource("/content/costoCero");
		Platos data = res.adaptTo(Platos.class);

		float actual = data.getCosto();
		assertEquals(expected, actual);
	}
	
	@Test
	void testGetWhithoutCosto() {
		final float expected = 0;
		Resource res = ctx.currentResource("/content/sinData");
		Platos data = res.adaptTo(Platos.class);

		float actual = data.getCosto();
		assertEquals(expected, actual);
	}

	@Test
	void testGetIcono() {
		final String expected = "/content/dam/theovnewicons/gaia.png";
		Resource res = ctx.currentResource("/content/lleno");
		Platos data = res.adaptTo(Platos.class);

		String actual = data.getIcono();
		assertEquals(expected, actual);
	}

	@Test
	void testGetAlergenos() {
		List<String> expected = new ArrayList<>();
		expected.add("alergeno1");
		expected.add("alergeno2");
		expected.add("alergeno3");
		expected.add("alergeno4");
		Resource res = ctx.currentResource("/content/lleno");
		Platos data = res.adaptTo(Platos.class);

		List<String> actual = data.getAlergenos();
		assertEquals(expected, actual);
	}
	@Test
	void testGetSomeAllergens() {
		List<String> expected = new ArrayList<>();
		expected.add("alergeno2");
		expected.add("alergeno4");
		Resource res = ctx.currentResource("/content/algunosAlergenos");
		Platos data = res.adaptTo(Platos.class);

		List<String> actual = data.getAlergenos();
		assertEquals(expected, actual);
	}
	
	@Test
	void testGetOneAllergen() {
		List<String> expected = new ArrayList<>();
		expected.add("alergeno2");
		Resource res = ctx.currentResource("/content/unAlergeno");
		Platos data = res.adaptTo(Platos.class);

		List<String> actual = data.getAlergenos();
		assertEquals(expected, actual);
	}

	@Test
	void testGetWithoutAllerges() {
		List<String> expected = new ArrayList<>();
		Resource res = ctx.currentResource("/content/sinData");
		Platos data = res.adaptTo(Platos.class);

		List<String> actual = data.getAlergenos();
		assertEquals(expected, actual);
	}
	
	@Test
	void testIsCostCeroFalse() {
		final boolean expected = false;
		Resource res = ctx.currentResource("/content/lleno");
		Platos data = res.adaptTo(Platos.class);

		boolean actual = data.isCostCero();
		assertEquals(expected, actual);
	}
	
	@Test
	void testIsCostCero() {
		final boolean expected = true;
		Resource res = ctx.currentResource("/content/costoCero");
		Platos data = res.adaptTo(Platos.class);

		boolean actual = data.isCostCero();
		assertEquals(expected, actual);
	}

	@Test
	void testEsVacioFalse() {
		final boolean expected = false;
		Resource res = ctx.currentResource("/content/lleno");
		Platos data = res.adaptTo(Platos.class);

		boolean actual = data.esVacio();
		assertEquals(expected, actual);
	}

	@Test
	void testEsVacio() {
		final boolean expected = true;
		Resource res = ctx.currentResource("/content/sinData");
		Platos data = res.adaptTo(Platos.class);

		boolean actual = data.esVacio();
		assertEquals(expected, actual);
	}
	
	@Test
	void tesEsVacioConUnElemento() {
		final boolean expected = false;
		Resource res = ctx.currentResource("/content/esVacio/platoConUnElemento");
		Platos data = res.adaptTo(Platos.class);

		boolean actual = data.esVacio();
		assertEquals(expected, actual);
	}
	@Test
	void tesEsVacioConDosElementos() {
		final boolean expected = false;
		Resource res = ctx.currentResource("/content/esVacio/platoConDosElementos");
		Platos data = res.adaptTo(Platos.class);

		boolean actual = data.esVacio();
		assertEquals(expected, actual);
	}
	@Test
	void tesEsVacioConTresElementos() {
		final boolean expected = false;
		Resource res = ctx.currentResource("/content/esVacio/platoConTresElementos");
		Platos data = res.adaptTo(Platos.class);

		boolean actual = data.esVacio();
		assertEquals(expected, actual);
	}
}
