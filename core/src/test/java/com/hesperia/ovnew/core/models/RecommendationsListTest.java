package com.hesperia.ovnew.core.models;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class RecommendationsListTest {
	public final AemContext ctx = new AemContext();
	
	@BeforeEach
	void setUp() throws Exception {
		ctx.load().json("/com/hesperia/ovnew/core/models/RecommendationsListTest.json", "/content");
	}

	@Test
	void constructors() {
		List<Recommendation> list = new ArrayList<>();
		list.add(new Recommendation("/content/dam/theovnewicons/2-ovnew-08.png",
				"TEN LA MENTE ABIERTA",
				"Ten la mente abierta a la experiencia. Las cosas se perciben mejor teniendo la mente receptiva."));
		list.add(new Recommendation("/content/dam/theovnewicons/2-ovnew-09.png",
				"DISFRUTA LA EXPERIENCIA",
				"Intenta no venir después de largas caminatas, de ejercicios o de la playa. Para disfrutar de Ovnew es importante que tengas cuerpo, mente y alma en total sintonía."));
		RecommendationsList expected = new RecommendationsList(list);
		Resource res = ctx.currentResource("/content/recommendations");
		RecommendationsList actual = new RecommendationsList(res.adaptTo(RecommendationsList.class));
		
		assertEquals(expected.toString(), actual.toString());
	}

	@Test
	void testGetRecommendations() {
		List<Recommendation> expected = new ArrayList<>();
		expected.add(new Recommendation("/content/dam/theovnewicons/2-ovnew-08.png",
				"TEN LA MENTE ABIERTA",
				"Ten la mente abierta a la experiencia. Las cosas se perciben mejor teniendo la mente receptiva."));
		expected.add(new Recommendation("/content/dam/theovnewicons/2-ovnew-09.png",
				"DISFRUTA LA EXPERIENCIA",
				"Intenta no venir después de largas caminatas, de ejercicios o de la playa. Para disfrutar de Ovnew es importante que tengas cuerpo, mente y alma en total sintonía."));
		Resource res = ctx.currentResource("/content/recommendations");
		//Recommendation actual = res.adaptTo(Recommendation.class);
		RecommendationsList actual = res.adaptTo(RecommendationsList.class);
		
		assertEquals(expected.toString(), actual.getRecommendations().toString());
	}

	@Test
	void testIsEmpty() {
		boolean expected = true;
		Resource res = ctx.currentResource("/content/recEmpty");
		RecommendationsList rec = res.adaptTo(RecommendationsList.class);
		boolean actual = rec.isEmpty();
		
		assertEquals(expected, actual);
	}
	
	@Test
	void testIsNotEmpty() {
		boolean expected = false;
		Resource res = ctx.currentResource("/content/recommendations");
		RecommendationsList rec = res.adaptTo(RecommendationsList.class);
		boolean actual = rec.isEmpty();
		
		assertEquals(expected, actual);
	}
}
