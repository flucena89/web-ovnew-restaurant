package com.hesperia.ovnew.core.models;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

@ExtendWith({ AemContextExtension.class, MockitoExtension.class })
class MenusImplTest {
	public final AemContext ctx = new AemContext();

	@BeforeEach
	void setUp() throws Exception {
		ctx.load().json("/com/hesperia/ovnew/core/models/MenusImplTest.json", "/content");
		ctx.load().json("/com/hesperia/ovnew/core/models/PlatosTest.json", "/child");

	}

	@Test
	void construct() {
		Resource res = ctx.currentResource("/child/plato2");
		ArrayList<String> allergen = new ArrayList<>();
		allergen.add("alergeno1");
		allergen.add("alergeno2");
		allergen.add("alergeno3");
		allergen.add("alergeno4");
		float cost = 0, costMenu = 160;
		Platos plato1 = new Platos("SER PRIMIGENIO", "El gusto y sus inicios", "- Cogollo a la brasa, hierbas y flores. Vinagreta oriental\n- Bulbos, tubérculos, cereales y fermentos\n- Crujiente de maíz\n-moringa, kimchi de rábanos y cerezas",
				cost, "/content/dam/theovnewicons/gaia.png", allergen);
		Platos plato2 = new Platos(res.adaptTo(Platos.class));
		res = ctx.currentResource("/child/plato3");
		Platos plato3 = new Platos(res.adaptTo(Platos.class));
		List<Platos> platos = new ArrayList<>();
		platos.add(plato1);
		platos.add(plato2);
		platos.add(plato3);
		
		MenusImpl expected = new MenusImpl("Nombre del menu", costMenu, "asdfasd", "sin PDF", "titulo del mensaje del menu", 
				"contenido del mensaje del menu", platos);
		res = ctx.currentResource("/content/menu");
		MenusImpl actual = new MenusImpl(res.adaptTo(MenusImpl.class));
		
		assertEquals(expected.toString(), actual.toString());
		
	}
	@Test
	void testGetTitulo() {
		final String expected = "Nombre del menu";
		Resource res = ctx.currentResource("/content/menu");
		MenusImpl data = res.adaptTo(MenusImpl.class);

		String actual = data.getTitulo();
		assertEquals(expected, actual);
	}

	@Test
	void testGetCosto() {
		final float expected = 160;
		Resource res = ctx.currentResource("/content/menu");
		MenusImpl data = res.adaptTo(MenusImpl.class);

		float actual = data.getCosto();
		assertEquals(expected, actual);	}

	@Test
	void testGetIcono() {
		final String expected = "asdfasd";
		Resource res = ctx.currentResource("/content/menu");
		MenusImpl data = res.adaptTo(MenusImpl.class);

		String actual = data.getIcono();
		assertEquals(expected, actual);
	}

	@Test
	void testGetPdf() {
		final String expected = "sin PDF";
		Resource res = ctx.currentResource("/content/menu");
		MenusImpl data = res.adaptTo(MenusImpl.class);

		String actual = data.getPdf();
		assertEquals(expected, actual);
	}

	@Test
	void testGetPlatos() {
		final int expected = 3;
		Resource res = ctx.currentResource("/content/menu");
		MenusImpl menu = res.adaptTo(MenusImpl.class);
		List<Platos> actual = menu.getPlatos();
				
 		assertEquals(expected, actual.size());
	}
	
	@Test
	void testSinPlatos() {
		final int expected = 0;
		Resource res = ctx.currentResource("/content/sinDatos");
		MenusImpl menu = res.adaptTo(MenusImpl.class);
		List<Platos> actual = menu.getPlatos();
				
 		assertEquals(expected, actual.size());
	}

	@Test
	void testGetTituloM() {
		final String expected = "titulo del mensaje del menu";
		Resource res = ctx.currentResource("/content/menu");
		MenusImpl data = res.adaptTo(MenusImpl.class);

		String actual = data.getTituloM();
		assertEquals(expected, actual);
	}

	@Test
	void testGetContenido() {
		final String expected = "contenido del mensaje del menu";
		Resource res = ctx.currentResource("/content/menu");
		MenusImpl data = res.adaptTo(MenusImpl.class);

		String actual = data.getContenido();
		assertEquals(expected, actual);
	}

	@Test
	void testGetHeightMenu() {
		final int expected = 3;
		Resource res = ctx.currentResource("/content/menu");
		MenusImpl menu = res.adaptTo(MenusImpl.class);
		int actual = menu.getHeightMenu();
				
 		assertEquals(expected, actual);
	}
	
	@Test
	void testHeightMenuCero() {
		final int expected = 0;
		Resource res = ctx.currentResource("/content/sinDatos");
		MenusImpl menu = res.adaptTo(MenusImpl.class);
		int actual = menu.getHeightMenu();
				
 		assertEquals(expected, actual);
	}

	@Test
	void testIsCostCero() {
		final boolean expected = true;
		Resource res = ctx.currentResource("/content/sinDatos");
		MenusImpl menu = res.adaptTo(MenusImpl.class);
		boolean actual = menu.isCostCero();
		
		assertEquals(expected, actual);
	}
	
	@Test
	void testIsCostCeroFalse() {
		final boolean expected = false;
		Resource res = ctx.currentResource("/content/menu");
		MenusImpl menu = res.adaptTo(MenusImpl.class);
		boolean actual = menu.isCostCero();
		
		assertEquals(expected, actual);
	}

	@Test
	void testEsVacioFalse() {
		final boolean expected = false;
		Resource res = ctx.currentResource("/content/menu");
		MenusImpl menu = res.adaptTo(MenusImpl.class);
		boolean actual = menu.esVacio();
		
		assertEquals(expected, actual);
	}
	
	@Test
	void testEsVacio() {
		final boolean expected = true;
		Resource res = ctx.currentResource("/content/sinDatos");
		MenusImpl menu = res.adaptTo(MenusImpl.class);
		boolean actual = menu.esVacio();
		
		assertEquals(expected, actual);
	}
	
	@Test
	void testNoEsVacioSinTitulo() {
		final boolean expected = false;
		Resource res = ctx.currentResource("/content/esVacioSinUnElemento");
		MenusImpl menu = res.adaptTo(MenusImpl.class);
		boolean actual = menu.esVacio();
		
		assertEquals(expected, actual);
	}
	
	@Test
	void testEsVacioSinDos() {
		final boolean expected = false;
		Resource res = ctx.currentResource("/content/esVacioSinDosElementos");
		MenusImpl menu = res.adaptTo(MenusImpl.class);
		boolean actual = menu.esVacio();
		
		assertEquals(expected, actual);
	}
	
	@Test
	void testHasMessageWithoutContent() {
		final boolean expected = false;
		Resource res = ctx.currentResource("/content/sinContenido");
		MenusImpl menu = res.adaptTo(MenusImpl.class);
		boolean actual = menu.hasMessage();
		
		assertEquals(expected, actual);
	}

	@Test
	void testHasMessage() {
		final boolean expected = true;
		Resource res = ctx.currentResource("/content/menu");
		MenusImpl menu = res.adaptTo(MenusImpl.class);
		boolean actual = menu.hasMessage();
		
		assertEquals(expected, actual);
	}
	
	@Test
	void testDoNotHasMessage() {
		final boolean expected = false;
		Resource res = ctx.currentResource("/content/sinDatos");
		MenusImpl menu = res.adaptTo(MenusImpl.class);
		boolean actual = menu.hasMessage();
		
		assertEquals(expected, actual);
	}

}
