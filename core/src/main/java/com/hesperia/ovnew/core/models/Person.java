package com.hesperia.ovnew.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables = { Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Person {

	@ValueMapValue
	private String name;
	@ValueMapValue
	private String description;
	@ValueMapValue
	private String instagram;
	@ValueMapValue
	private String image;
	@ValueMapValue
	private String twitter;
	@ValueMapValue
	private String facebook;
	
	
	

	public Person() {
	}

	public Person(Person person) {
		this.name = person.name;
		this.description = person.description;
		this.instagram = person.instagram;
		this.image = person.image;
		this.twitter = person.twitter;
		this.facebook = person.facebook;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the instagram
	 */
	public String getInstagram() {
		return instagram;
	}

	/**
	 * @return the Image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @return the twitter
	 */
	public String getTwitter() {
		return twitter;
	}

	/**
	 * @return the facebook
	 */
	public String getFacebook() {
		return facebook;
	}




}
