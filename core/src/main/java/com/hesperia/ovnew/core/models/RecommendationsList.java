package com.hesperia.ovnew.core.models;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

@Model(adaptables = { Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class RecommendationsList {
	/*************
	 * PROPERTIES
	 *************/
	@Inject
	@Named("recomList")
	List<Recommendation> recommendations;

	/*************
	 * CONSTRUCTORS
	 *************/
	public RecommendationsList() {
		this.recommendations = new ArrayList<>();
	}
	
	public RecommendationsList(List<Recommendation> list) {
		this.recommendations = list;
	}
	
	public RecommendationsList(RecommendationsList list) {
		this.recommendations = list.recommendations;
	}
	
	/*************
	 * GETTERS
	 *************/
	public List<Recommendation> getRecommendations(){
		return this.recommendations;
	}
	
	public boolean isEmpty() {
		if (recommendations.size()==0) {
			return true;
		}else {
			return false;
		}
	}
	
	@Override
	public String toString() {
		String all = "[";
		for (Recommendation recommendation : recommendations) {
			all = all+"\n"+recommendation.toString()+",";
		}
		all = all.substring(0, all.length()-1)+"\n]";
		
		return all;
	}
}
