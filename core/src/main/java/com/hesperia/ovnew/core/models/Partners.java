package com.hesperia.ovnew.core.models;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

@Model(adaptables = { Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Partners {

	@Inject
	@Named("nombre")
	private List<ImagenUrl> url = new ArrayList<>();

	public Partners() {
	}

	public Partners(Partners url) {
		this.url = url.url;
	}

	/**
	 * @return the url
	 */
	public List<ImagenUrl> getImg() {
		return url;
	}

}