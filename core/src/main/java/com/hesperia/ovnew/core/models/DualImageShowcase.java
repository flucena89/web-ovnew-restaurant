package com.hesperia.ovnew.core.models;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Model(adaptables = { Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class DualImageShowcase {
	
Logger log = LoggerFactory.getLogger(this.getClass());
	
	/**************************
	 * PROPERTIES TAB GENERAL
	 **************************/
	@Inject
	@Named("customCssClass")
	private String customCssClass;
	
	@Inject
	@Named("imageForeground")
	private String imageForeground;
	
	@Inject
	@Named("imageBackground")
	private String imageBackground;
	
	@Inject
	@Named("enableParallax")
	private String enableParallax;
	
	@Inject
	@Named("yAxisTranslationForegroundImage")
	private Integer yAxisTranslationForegroundImage;
	
	@Inject
	@Named("yAxisTranslationBackgroundImage")
	private Integer yAxisTranslationBackgroundImage;
	
	@Inject
	@Named("imagesPosition")
	private String imagesPosition;
	
	@Inject
	@Named("contentAlignment")
	private String contentAlignment;
	
	@Inject
	@Named("title")
	private String title;
	
	@Inject
	@Named("subtitle")
	private String subtitle;
	
	@Inject
	@Named("text")
	private String text;
	
	@Inject
	@Named("enableButton")
	private String enableButton;
	
	@Inject
	@Named("buttonText")
	private String buttonText;

	@Inject
	@Named("customLink")
	private String customLink;
	
	@Inject
	@Named("customLinkTarget")
	private String customLinkTarget;
	
	/**************************
	 * PROPERTIES TAB TITLE STYLES
	 **************************/
	@Inject
	@Named("titleTag")
	private String titleTag;
	
	@Inject
	@Named("titleColor")
	private String titleColor;
	
	@Inject
	@Named("titleTopMargin")
	private Integer titleTopMargin;

	@Inject
	@Named("titleBottomMargin")
	private Integer titleBottomMargin;
	
	/**************************
	 * PROPERTIES TAB SUBTITLE STYLES
	 **************************/
	@Inject
	@Named("subtitleTag")
	private String subtitleTag;
	
	@Inject
	@Named("subtitleColor")
	private String subtitleColor;
	
	/**************************
	 * PROPERTIES TAB TEXT STYLES
	 **************************/
	@Inject
	@Named("textColor")
	private String textColor;
	
	@Inject
	@Named("textTopMargin")
	private Integer textTopMargin;
	
	public DualImageShowcase(){}
	
	public DualImageShowcase(DualImageShowcase dualimageshowcase) {
		/********************
		 * GENERAL
		 ********************/
		this.customCssClass = dualimageshowcase.customCssClass;
		this.imageForeground = dualimageshowcase.imageForeground;
		this.imageBackground = dualimageshowcase.imageBackground;
		this.enableParallax = dualimageshowcase.enableParallax;
		this.yAxisTranslationForegroundImage = dualimageshowcase.yAxisTranslationForegroundImage;
		this.yAxisTranslationBackgroundImage = dualimageshowcase.yAxisTranslationBackgroundImage;
		this.imagesPosition = dualimageshowcase.imagesPosition;
		this.title = dualimageshowcase.title;
		this.subtitle = dualimageshowcase.subtitle;
		this.text = dualimageshowcase.text;
		this.buttonText = dualimageshowcase.buttonText;
		this.customLink = dualimageshowcase.customLink;
		this.customLinkTarget = dualimageshowcase.customLinkTarget;
		/********************
		 * TITLE STYLES
		 ********************/
		this.titleTag = dualimageshowcase.titleTag;
		this.titleColor = dualimageshowcase.titleColor;
		this.titleTopMargin = dualimageshowcase.titleTopMargin;
		this.titleBottomMargin = dualimageshowcase.titleBottomMargin;
		/********************
		 * SUBTITLE STYLES
		 ********************/
		this.subtitleTag = dualimageshowcase.subtitleTag;
		this.subtitleColor = dualimageshowcase.subtitleColor;
		/********************
		 * TEXT STYLES
		 ********************/
		this.textColor = dualimageshowcase.textColor;
		this.textTopMargin = dualimageshowcase.textTopMargin;
	}
	
	/*************
	 * GETTERS GENERAL
	 *************/
	public String getCustomCssClass() {
		return customCssClass;
	}
	
	public String getImageForeground() {
		return imageForeground;
	}
	
	public String getImageBackground() {
		return imageBackground;
	}
	
	public String getEnableParallax() {
		return enableParallax;
	}
	
	public Integer getYAxisTranslationForegroundImage() {
		return yAxisTranslationForegroundImage;
	}
	
	public Integer getYAxisTranslationBackgroundImage() {
		return yAxisTranslationBackgroundImage;
	}
	
	public String getImagesPosition() {
		return imagesPosition;
	}
	
	public String getContentAlignment() {
		return contentAlignment;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getSubtitle() {
		return subtitle;
	}
	
	public String getText() {
		return text;
	}
	
	public String getEnableButton() {
		return enableButton;
	}
	
	public String getButtonText() {
		return buttonText;
	}
	
	public String getCustomLink() {
		return customLink;
	}
	
	public String getCustomLinkTarget() {
		return customLinkTarget;
	}
	
	/*************
	 * GETTERS TITLE STYLES
	 *************/
	public String getTitleTag() {
		return titleTag;
	}
	
	public String getTitleColor() {
		return titleColor;
	}
	
	public Integer getTitleTopMargin() {
		return titleTopMargin;
	}
	
	public Integer getTitleBottomMargin() {
		return titleBottomMargin;
	}
	
	/*************
	 * GETTERS SUBTITLE STYLES
	 *************/
	public String getSubtitleTag() {
		return subtitleTag;
	}
	
	public String getSubtitleColor() {
		return subtitleColor;
	}
	
	/*************
	 * GETTERS TEXT STYLES
	 *************/
	public String getTextColor() {
		return textColor;
	}
	
	public Integer getTextTopMargin() {
		return textTopMargin;
	}
	
	 public boolean isEmpty() {
	    	return StringUtils.isBlank(imageForeground)
	    			&& StringUtils.isBlank(imageBackground)
	    			&& StringUtils.isBlank(title)
	    			&& StringUtils.isBlank(subtitle)
	    			&& StringUtils.isBlank(text);
	 }
}
