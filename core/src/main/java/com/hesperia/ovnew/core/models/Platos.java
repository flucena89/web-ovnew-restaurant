package com.hesperia.ovnew.core.models;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;


@Model(adaptables = { Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Platos {

	/*************
	 * PROPERTIES
	 *************/
	@Inject
	@Named("tituloPlato")
	private String titulo;
	
	@Inject
	@Named("descripcionPlato")
	private String descripcion;
	
	@Inject
	@Named("detallePlato")
	private String detalles;
	
	@Inject
	@Named("costoPlato")
	private float costo;
		
	@Inject
	@Named("iconPlato")
	private String icono;
	
	@ValueMapValue
	private List<String> alergenos;

	/*************
	 * CONSTRUCTORS
	 *************/
	public Platos() {
		titulo = "";
		descripcion = "";
		detalles = "";
		costo = 0;
		icono = "";
		alergenos = new ArrayList<>();
	}

	public Platos(String nombre, String desc, String detalle, Float cost, String icon, ArrayList<String> alergenos) {
		titulo = nombre;
		descripcion = desc;
		detalles = detalle;
		costo = cost;
		this.icono = icon;
		this.alergenos = alergenos;
	}
	
	public Platos(Platos plat) {
		titulo = plat.titulo;
		descripcion = plat.descripcion;
		detalles = plat.detalles;
		costo = plat.costo;
		icono = plat.icono;
		alergenos = plat.alergenos;
	}
	
	/*************
	 * GETTERS
	 *************/
	public String getTitulo() {
		return titulo;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public List<String> getDetalles() {
		List<String> retorno = new ArrayList<>();
		String[] evaluar = detalles.trim().split("\n");
		for (String index : evaluar) {
			retorno.add(index);
		}
		return retorno;
	}
	
	public float getCosto() {
		return costo;
	}
	
	public String getIcono() {
		return icono;
	}
	
	public List<String> getAlergenos() {
		return alergenos; 
	}
	
	public boolean isCostCero() {
		return (costo==0);
	}
	
	public boolean esVacio() {
		if ((titulo == "")&&(descripcion=="")&&(detalles=="")&&(icono=="")) {
			return true;
		}
		return false;
	}
	
	@Override
	public String toString() {
		String content = "["+this.titulo+","+this.descripcion+","+this.detalles+","+this.costo+","+this.icono
				+",\n"+this.alergenos.toString()+"]";
		return content;
	}
}
