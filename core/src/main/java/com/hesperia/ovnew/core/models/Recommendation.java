package com.hesperia.ovnew.core.models;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

@Model(adaptables = { Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Recommendation {
	/*************
	 * PROPERTIES
	 *************/
	@Inject
	@Named("recommendationIcon")
	private String icon;
	
	@Inject
	@Named("recommendationTitle")
	private String title;
	
	@Inject
	@Named("recommendationDescription")
	private String description;

	/*************
	 * CONSTRUCTORS
	 *************/
	public Recommendation() {
		this.icon = "";
		this.title = "";
		this.description = "";
	}
	
	public Recommendation(String icon, String title, String desc) {
		this.icon = icon;
		this.title = title;
		this.description = desc;
	}
	
	public Recommendation(Recommendation rec) {
		this.icon = rec.icon;
		this.title = rec.title;
		this.description = rec.description;
	}
	
	/*************
	 * GETTERS
	 *************/
	public String getIcon() {
		return this.icon;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public boolean isEmpty() {
		return (this.title=="" && this.description =="");
	}
	
	@Override
	public String toString() {
		return ("["+this.icon+","+this.title+","+this.description+"]");
	}
}
