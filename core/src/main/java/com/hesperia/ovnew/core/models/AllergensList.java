package com.hesperia.ovnew.core.models;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables = { Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class AllergensList {

	@Inject
	@Named("ingredientes")
	private List<Allergen> alergenos;
	
	@ValueMapValue
	private String titleComponent;
	
	
	public AllergensList() {
		
	}

	public AllergensList(AllergensList alergenos) {
		this.alergenos = alergenos.alergenos;
		this.titleComponent = alergenos.titleComponent;
	}

	/**
	 * @return the alergeno
	 */
	public List<Allergen> getAlergenos() {
		return alergenos;
	}

	/**
	 * @return the titleComponent
	 */
	public String getTitleComponent() {
		return titleComponent;
	}


	
}
