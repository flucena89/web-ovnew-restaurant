package com.hesperia.ovnew.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables = { Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class CircularCarouselImage {

	@ValueMapValue
	private String imageCarousel;

	public CircularCarouselImage() {
	}

	public CircularCarouselImage(CircularCarouselImage image) {
		this.imageCarousel = image.imageCarousel;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return imageCarousel;
	}

}
