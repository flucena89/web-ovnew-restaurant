package com.hesperia.ovnew.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables = { Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Allergen {

	@ValueMapValue
	private String title;
	
	@ValueMapValue
	private String description;
	
	public Allergen() {
		
	}

	/**
	 * @param title
	 * @param description
	 */
	public Allergen(Allergen allergen) {
		this.title = allergen.title;
		this.description = allergen.description;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
}
