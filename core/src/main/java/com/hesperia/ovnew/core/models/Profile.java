package com.hesperia.ovnew.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables = { Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Profile {


	
	@ValueMapValue
	private String redSocial;
	
	@ValueMapValue
	private String url;
	

	
	public Profile() {

	}
	
	public Profile (Profile profile) {
		this.redSocial = profile.redSocial;
		this.url = profile.url;
	}

	/**
	 * @return the redSocial
	 */
	public String getRedSocial() {
		return redSocial;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url.trim();
	}

}
