package com.hesperia.ovnew.core.models;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

@Model(adaptables = { Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class CircularCarousel {

	@Inject
	@Named ("images")
	private List<CircularCarouselImage> img = new ArrayList<>();
	
	private String algo = "asdasdasd";

	/**
	 * @return the algo
	 */
	public String getAlgo() {
		return algo;
	}

	public CircularCarousel() {

	}

	public CircularCarousel(CircularCarousel images) {
		this.img = images.img;
	}

	/**
	 * @return the images
	 */
	public List<CircularCarouselImage> getImages() {
		return img;
	}

}
