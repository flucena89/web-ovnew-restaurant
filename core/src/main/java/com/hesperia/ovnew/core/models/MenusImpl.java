package com.hesperia.ovnew.core.models;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;


@Model(adaptables = { Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class MenusImpl{

	@Inject
	@Named("tituloMenu")
	private String titulo;
	
	@Inject
	@Named("costoMenu")
	private float costo;
	
	@Inject
	@Named("iconMenu")
	private String icono;
	
	@Inject
	@Named("fileReference")
	private String pdf;
	
	@Inject
	@Named("campoPlatos")
	private List<Platos> platos;
	
	@Inject
	@Named("tituloMensaje")
	private String tituloM;
	
	@Inject
	@Named("contenidoMensaje")
    private String contenido;
	
	/***************
	 * CONSTRUCTORES
	 ***************/
	public MenusImpl() {
		this.titulo = "";
		this.costo = 0;
		this.icono = "";
		this.pdf = "";
		this.tituloM = "";
		this.contenido = "";
		this.platos = new ArrayList<>();
	}
	
	public MenusImpl(String nombre, float cost, String icon, String pdf, String tituloM, 
			String contenido, List<Platos> platos) {
		this.titulo = nombre;
		this.costo = cost;
		this.icono = icon;
		this.pdf = pdf;
		this.tituloM = tituloM;
		this.contenido = contenido;
		this.platos = platos;
	}
	
	public MenusImpl(MenusImpl men) {
		this.titulo = men.titulo;
		this.costo = men.costo;
		this.icono = men.icono;
		this.pdf = men.pdf;
		this.platos = men.platos;
		this.tituloM = men.tituloM;
		this.contenido = men.contenido;
	}
	
	/*********
	 * GETTERS
	 *********/
	public String getTitulo() {
		return this.titulo;
	}
	
	public float getCosto() {
		return this.costo;
	}
	
	public String getIcono() {
		return this.icono;
	}
	
	public String getPdf(){
		return this.pdf;
	}
	
	public List<Platos> getPlatos(){
		return this.platos;
	}
	
	public String getTituloM() {
		return this.tituloM;
	}
	
	public String getContenido() {
		return this.contenido;
	}
	
	public int getHeightMenu() {
		return this.platos.size();
	}
	
	/*************
	 * VALIDATORS
	 *************/
	public boolean isCostCero() {
		return (this.costo==0);
	}
	
	public boolean esVacio() {
		return ((titulo =="")&&(icono =="")&&(costo==0));
	}
	/*
	 * se puede tener un titulo o solo contenido para el mensaje
	 */
	public boolean hasMessage() {
		return (tituloM != "" && contenido != "");
	}

	@Override
	public String toString() {
		String content = "["+this.titulo+","+this.costo+","+icono+","+this.pdf+","+this.tituloM
				+","+this.contenido+",\n"+this.platos.toString()+"]";
		return content;
	}
}