package com.hesperia.ovnew.core.models;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

@Model(adaptables = { Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SocialNetworks {

	@Inject
	@Named("redes")
	private List<Profile> redes = new ArrayList<>();
	
	public SocialNetworks() {
		
	}
	
	public SocialNetworks(SocialNetworks socialNetworks) {
		this.redes = socialNetworks.redes;
	}
	
	/**
	 * @return the redes
	 */
	public List<Profile> getRedes() {
		return redes;
	}
}
