package com.hesperia.ovnew.core.models;

import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables = { Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ContentBox {

	@ValueMapValue
	private String image;

	@ValueMapValue
	private String title;

	@ValueMapValue
	private String description;

	@ValueMapValue
	private String address;
	
	@ValueMapValue
	private String addressUrl;

	@ValueMapValue
	private String email;

	@ValueMapValue
	private String phone;

	@ValueMapValue
	private String buttonUrl;

	@ValueMapValue
	private String buttonText;

	@ValueMapValue
	private String showButton;

	public ContentBox() {

	}

	public ContentBox(String image, String title, String description, String address, String email, String phone,
			String buttonUrl, String buttonText, String showButton, String addressUrl) {
		this.image = image;
		this.title = title;
		this.description = description;
		this.address = address;
		this.email = email;
		this.phone = phone;
		this.buttonUrl = buttonUrl;
		this.buttonText = buttonText;
		this.showButton = showButton;
		this.addressUrl= addressUrl;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @return the addresUrl
	 */
	public String getAddressUrl() {
		return addressUrl;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @return the buttonUrl
	 */
	public String getButtonUrl() {
		return buttonUrl;
	}

	/**
	 * @return the buttonText
	 */
	public String getButtonText() {
		return buttonText;
	}

	/**
	 * @return the showButton
	 */
	public String getShowButton() {
		return showButton;
	}

}
