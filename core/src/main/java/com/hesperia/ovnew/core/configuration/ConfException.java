package com.hesperia.ovnew.core.configuration;

public class ConfException extends Exception {
	private static final long serialVersionUID = 1L;

	public ConfException() {
		super();
	}

	public ConfException(String message, Throwable cause) {
		super(message, cause);
	}

	public ConfException(String message) {
		super(message);
	}

	public ConfException(Throwable cause) {
		super(cause);
	}
}