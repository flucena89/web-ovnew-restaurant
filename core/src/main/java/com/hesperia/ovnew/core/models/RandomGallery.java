package com.hesperia.ovnew.core.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Model(adaptables = { Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class RandomGallery {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	/*************
     * PROPERTIES
     *************/
	 @Inject
	 @Named("imagesList")
	 private List<ImageRandom> images = new ArrayList<>();
	 
	 private String algo = "algo";
	 
	 public RandomGallery(){
	 }
	 
	 public RandomGallery(RandomGallery randomgallery) {
		 this.images = randomgallery.images;
	 }
	 
	 /*************
     * INITIALIZATION
     *************/
    @PostConstruct
    protected void init() {
    	Collections.shuffle(images);
    }
	 
	 /*************
     * GETTERS
     *************/	 
	 public List<ImageRandom> getImages() {
		 return images.stream().map(ImageRandom::new).collect(Collectors.toList());
	 }
	 
	 public ArrayList<String> getArrayListImages() {
		 
		 ArrayList<String> arrayList = new ArrayList<String>();
		 
		 for(int i=0;i<images.size();i++) {
			 if(i==images.size()-1) {
				 arrayList.add(images.get(i).getImage());
			 }else {
				 arrayList.add(images.get(i).getImage()+"@#key#@");
			 }
			 
		 }
		 
		 
		 
		 return arrayList;
	 }

	/**
	 * @return the algo
	 */
	public String getAlgo() {
		return algo;
	}

}
