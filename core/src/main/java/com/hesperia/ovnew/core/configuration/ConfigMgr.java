package com.hesperia.ovnew.core.configuration;

import java.util.Dictionary;

import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(immediate = true)
@Designate(ocd = ConfigMgr.Config.class)
public class ConfigMgr {

	/*
	 * Parámetros de configuración
	 */
	@ObjectClassDefinition(name = "Ovnew Restaurant Web Configuration", description = "Configuration parameters for Ovnew Restaurant Web")
	public static @interface Config {

		public static final String PHONE_LINK = "contact.phone.link";
		public static final String EMAIL = "contact.email";
		public static final String SCHEDULE = "schedule";
		public static final String DURATION = "duration";
		public static final String PLACE_URL = "place.url";
		public static final String PLACE_MAPS_DATA = "place.maps.data";
		public static final String ADDRESS = "address";
		public static final String CONTACT_EMAIL_EVENT = "contact.email.event";
		public static final String GOOGLE_MAPS_APIKEY = "google.maps.apikey";

		@AttributeDefinition(name = "Email", description = "Contact email", type = AttributeType.STRING)
		String contact_email();

		@AttributeDefinition(name = "Phone link", description = "Contact phone link", type = AttributeType.STRING)
		String contact_phone_link();

		@AttributeDefinition(name = "Schedule", description = "Restaurant schedule", type = AttributeType.STRING)
		String schedule();

		@AttributeDefinition(name = "Duration", description = "Experience duration", type = AttributeType.STRING)
		String duration();

		@AttributeDefinition(name = "Place URL", description = "Place URL on google maps", type = AttributeType.STRING)
		String place_url();

		@AttributeDefinition(name = "Place Maps Data", description = "Place Maps Data to set location marker", type = AttributeType.STRING)
		String place_maps_data();

		@AttributeDefinition(name = "Address", description = "Restaurant Address", type = AttributeType.STRING)
		String address();

		@AttributeDefinition(name = "Contact Email Event", description = "Contact Email Event", type = AttributeType.STRING)
		String contact_email_event();

		@AttributeDefinition(name = "Google Maps API_KEY", description = "Google Maps API_KEY", type = AttributeType.STRING)
		String google_maps_apikey();

		/** Adobe Launch (tag manager) */ // (pending)
		/*
		 * public static final String LAUNCH_ENV_ID = "launch.env.id";
		 * 
		 * @AttributeDefinition( name = "Launch Environment ID", description =
		 * "Launch Environment ID, including environment: EN2bdac151cdde446b9a986e4d3cb59756-development"
		 * , type = AttributeType.STRING) String launch_env_id();
		 */
	}

	/*
	 * Configuration Manager
	 */
	private static final Logger log = LoggerFactory.getLogger(ConfigMgr.class);
	private static Dictionary<?, ?> properties;

	/**
	 * Método a ejecutar en la activación del bundle. Obtiene las propiedades del
	 * contexto y las guarda en la variable de classe.
	 * 
	 * @param context
	 */
	protected void activate(ComponentContext context) {
		log.info("Entering ConfigurationManager activate()");
		properties = context.getProperties();
		log.info("Exiting ConfigurationManager activate()");
	}

	/**
	 * Método a ejecutar en la desactivación del bundle.
	 * 
	 * @param context
	 */
	protected void deactivate(ComponentContext context) {
		log.info("ConfigurationManager deactivate()");
	}

	public static Object getProperty(String propertyName) throws ConfException {
		Object object = properties.get(propertyName);
		if (object == null) {
			throw new ConfException("Propiedad " + propertyName + " no definida.");
		}
		return object;
	}

	public static String getStringProperty(String propertyName) throws ConfException {
		return PropertiesUtil.toString(getProperty(propertyName), "");
	}

	public static int getIntegerProperty(String propertyName) throws ConfException {
		return PropertiesUtil.toInteger(getProperty(propertyName), 0);
	}

	public static long getLongProperty(String propertyName) throws ConfException {
		return PropertiesUtil.toLong(getProperty(propertyName), 0);
	}

	public static Boolean getBooleanProperty(String propertyName) throws ConfException {
		return PropertiesUtil.toBoolean(getProperty(propertyName), true);
	}

	public String getDuration() throws ConfException {
		return getStringProperty(Config.DURATION);
	}

	public String getGoogleApiKey() throws ConfException {
		return getStringProperty(Config.GOOGLE_MAPS_APIKEY);
	}

	public String getAddress() throws ConfException {
		return getStringProperty(Config.ADDRESS);
	}

	public String getPhoneLink() throws ConfException {
		return getStringProperty(Config.PHONE_LINK);
	}

	public String getEmail() throws ConfException {
		return getStringProperty(Config.EMAIL);
	}

	public String getSchedule() throws ConfException {
		return getStringProperty(Config.SCHEDULE);
	}

	public String getPlaceUrl() throws ConfException {
		return getStringProperty(Config.PLACE_URL);
	}

	public String getContactEmailEvent() throws ConfException {
		return getStringProperty(Config.CONTACT_EMAIL_EVENT);
	}

	public String getFormatedPhone() throws ConfException {
		String phone = getStringProperty(Config.PHONE_LINK);
		String code = phone.substring(0, 3);
		String zone = phone.substring(3);
		return code + ' ' + zone;
	}

	public String getPlaceMapsData() throws ConfException {
		return getStringProperty(Config.PLACE_MAPS_DATA);
	}
}
