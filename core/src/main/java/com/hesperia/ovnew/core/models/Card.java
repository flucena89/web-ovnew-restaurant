package com.hesperia.ovnew.core.models;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables = { Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Card {

	@Inject
	@Named("profile")
	private List<Person> person = new ArrayList<>();

	@ValueMapValue
	private String title;
	
	@ValueMapValue
	private String subtitle;

	public Card() {
	}

	public Card(Card card) {
		this.person = card.person;
		this.title = card.title;
		this.subtitle = card.subtitle;
	}

	/**
	 * @return the name
	 */
	public List<Person> getPerson() {

		return person;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @return the subtitle
	 */
	public String getSubtitle() {
		return subtitle;
	}


}
