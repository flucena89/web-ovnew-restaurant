package com.hesperia.ovnew.core.models;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

@Model(adaptables = { Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ImagenUrl {

	@Inject
	@Named("imagen")
	private String img;
	
	@Inject
	@Named("type")
	private String type;

	public ImagenUrl() {
	}

	public ImagenUrl(ImagenUrl url) {
		this.img = url.img;
		this.type = url.type;
	}

	public String getUrl() {
		return img;
	}
	public String getType() {
		return type;
	}
}