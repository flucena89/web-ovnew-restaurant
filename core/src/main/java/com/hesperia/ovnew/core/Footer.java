package com.hesperia.ovnew.core;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;

import com.hesperia.ovnew.core.models.Base;

@Model(adaptables = { Resource.class, SlingHttpServletRequest.class })
public class Footer extends Base {

}
