package com.hesperia.ovnew.core.models;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Model(adaptables = { Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ImageRandom {
Logger log = LoggerFactory.getLogger(this.getClass());
	
	/*************
	 * PROPERTIES
	 *************/
	@Inject
	@Named("image")
	private String image;
	
	
	public ImageRandom(){}
	
	public ImageRandom(ImageRandom imagerandom) {
		this.image = imagerandom.image;
	}
	
	/*************
	 * GETTERS
	 *************/
	public String getImage() {
		return image;
	}

}
