package com.hesperia.ovnew.core.models;

import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;

import com.day.cq.wcm.api.Page;

public class Base {

	@ScriptVariable
	private Page currentPage;
	private Page home;

	private String telefonoEnlace = "+34937070760";
	private String telefonoFormato = "+34 937070760";
	private String email = "contactanos@theovnew.com";
	private String schedule = "Martes a Sábados 9:00 PM\n";
	private String duration = "Duración de la experiencia: 2 horas";
	private String paginaReservasPorDefecto = "https://www.theovnew.com/reservas/";
	private String addressLink = "https://www.google.com/maps/place/Hotel+Hesperia+Barcelona+Tower/@41.346028,2.1062911,17z/data=!3m1!4b1!4m5!3m4!1s0x12a498d43a550597:0x5ed87fe4148219a8!8m2!3d41.346028!4d2.1084798";
	private String address = "Gran Via, 144, Hospitalet de Llobregat 08907	Hospitalet de Llobregat Barcelona– España.";
	private String emailEvent = "lfernandez@hesperia.com";

	/**
	 * @return the telefonoEnlace
	 */
	public String getTelefonoEnlace() {
		return telefonoEnlace;
	}

	/**
	 * @return the telefonoFormato
	 */
	public String getTelefonoFormato() {
		return telefonoFormato;
	}

	/**
	 * @return the paginaReservasPorDefecto
	 */
	public String getPaginaReservasPorDefecto() {
		return paginaReservasPorDefecto;
	}

	public String getPath() {
		return (currentPage != null ? currentPage.getPath() : "") + "----";
	}

	public Page getHome() {
		return home;
	}

	public String getEmail() {
		return email;
	}

	public String getSchedule() {
		return schedule;
	}

	public String getDuration() {
		return duration;
	}

	public String getAddressLink() {
		return addressLink;
	}

	public void setAddressLink(String addressLink) {
		this.addressLink = addressLink;
	}

	public String getAddress() {
		return address;
	}

	public String getEmailEvent() {
		return emailEvent;
	}

}
